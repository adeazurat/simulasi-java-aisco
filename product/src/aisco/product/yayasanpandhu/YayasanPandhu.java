package aisco.product.yayasanpandhu;

import aisco.product.selection.FeatureSelection;
import aisco.program.activity.Program;
import aisco.program.activity.ProgramFactory;
import aisco.program.activity.ProgramImpl;
import aisco.financialreport.core.FinancialReport;
import aisco.financialreport.core.FinancialReportFactory;
import aisco.financialreport.income.FinancialReportImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class YayasanPandhu {

    public static void main(String[] args) {

        //Select features by calling default constructor
        //Product Yayasan Pandhu consists of Feature (Activity, Income)
        FeatureSelection features = new FeatureSelection();
        features.addFeatures(ProgramFactory.createProgram("aisco.program.activity.ProgramImpl"));
        features.addFeatures(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl"));


        //Product Validation (Check Feature's constraints)
        try {
            features.productValidation("Yayasan Pandhu");
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(10);
        }

        //Create instance of Program
        Program bingkisanTHR = new ProgramImpl(1, "Berbagi Bingkisan THR", "Berbagi bingkisan jelang hari raya idul fitri untuk Tunawisma", "50 orang", "Relawan Pandhu", "https://www.yaysanpandhu.splelive.id/pandhu.jpg");
        Program lombapanti = new ProgramImpl(1, "Lomba Antar Panti", "Lomba keagaaman antar panti asuhan di Daerah Karawang", "30 Panti Asuhan", "Relawan Pandhu", "https://www.yaysanpandhu.splelive.id/panti.jpg");

        //Create instance of Incomes
        List<FinancialReport> incomes = new ArrayList<>();
        FinancialReport income1 = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "1", "12-11-2019", 2500000, "Sumbangan Warga RT 11", bingkisanTHR, "11000", "Cash");
        FinancialReport income2 = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "2", "13-11-2019", 300000, "Sumbangan Pak Raden", lombapanti, "11000", "Transfer");
        FinancialReport income3 = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "3", "14-11-2019", 500000, "Sumbangan Anonim", bingkisanTHR, "110", "Transfer");
        incomes.add(income1);
        incomes.add(income2);
        incomes.add(income3);
        System.out.println(Arrays.asList(incomes));


    }
}
