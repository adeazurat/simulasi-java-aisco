package aisco.product.charityschool;

import aisco.product.selection.FeatureSelection;
import aisco.program.activity.Program;
import aisco.program.activity.ProgramFactory;
import aisco.financialreport.core.FinancialReport;
import aisco.financialreport.core.FinancialReportFactory;
import aisco.financialreport.income.FinancialReportImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CharitySchool {
    FeatureSelection features;
    List<Object> featuresList;

    public static void main(String[] args) {

        //Select features by calling default constructor
        //Product Charity School consists of Feature (Activity, Income, Expense)
        FeatureSelection features = new FeatureSelection();
        features.addFeatures(ProgramFactory.createProgram("aisco.program.activity.ProgramImpl"));
        features.addFeatures(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl"));
        features.addFeatures(FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl"));


        //Product Validation (Check Feature's constraints)
        try {
            features.productValidation("Charity School");
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(10);
        }

        //Create instance of program
        Program bangunsekolah = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl", 1, "Bangun Sekolah", "Membangun sekolah untuk anak kurang mampu", "100 orang", "Sekolah Alami", "https://www.sekolahku.splelive.id/logosekolah");
        Program kelasgratis = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl", 2, "Belajar Gratis", "Program Belajar Gratis untuk anak usia sekolah dasar", "100 orang", "Sekolah Alami", "https://www.sekolahku.splelive.id/logokelasgratis");
        Program trainingguru = ProgramFactory.createProgram("aisco.program.internal.ProgramImpl", 3, "Training Guru", "Training untuk Guru Baru yang akan mengajar", "10 Guru");

        //Create instance of Incomes
        List<FinancialReport> incomes = new ArrayList<>();
        FinancialReport income1 = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "1", "23-10-2019", 100000, "Sumbangan Bu Nana", bangunsekolah, "11000", "Transfer");
        FinancialReport income2 = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "2", "24-10-2019", 300000, "Sumbangan Pak Joni", kelasgratis, "11000", "Cash");
        FinancialReport income3 = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "3", "11-11-2019", 5000000, "CSR PT Sejahtera", bangunsekolah, "110", "Transfer");
        FinancialReport income4 = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", "3", "11-11-2019", 50000, "Sumbangan Aida", trainingguru, "110", "Cash");
        incomes.add(income1);
        incomes.add(income2);
        incomes.add(income3);
        incomes.add(income4);
        System.out.println(Arrays.asList(incomes));
        FinancialReportImpl incomekami = (FinancialReportImpl) income1;
        incomekami.printTotalIncome();

        //Create instance of Expenses
        List<FinancialReport> expenses = new ArrayList<>();
        FinancialReport expense1 = FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl", "10", "23-10-2019", 1000000, "Pembelian Kursi", bangunsekolah, "41000");
        FinancialReport expense2 = FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl", "20", "24-10-2019", 100000, "Konsumsi Guru", kelasgratis, "410");
        FinancialReport expense3 = FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl", "30", "12-10-2019", 50000, "Pembelian Spidol", kelasgratis, "41000");
        expenses.add(expense1);
        expenses.add(expense2);
        expenses.add(expense3);
        System.out.println(Arrays.toString(expenses.toArray()));

    }
}
