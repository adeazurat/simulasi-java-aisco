package aisco.product.selection;

import java.util.ArrayList;
import java.util.List;

public class FeatureSelection {
    List<Object> featuresList;

    public FeatureSelection()
    {
        featuresList = new ArrayList<>();
    }


    public void printListFeatures(String product)
    {
        System.out.println("Product " + product + " is succesfully created.");
        System.out.print("Features Selection: ");
        for (Object list:featuresList) {
            if (list.equals("aisco.financialreport.income.FinancialReportImpl")) {
                System.out.print("Income Report ");
            } else if (list.equals("aisco.financialreport.expense.FinancialReportImpl")) {
                System.out.print("Expense Report ");
            } else if (list.equals("aisco.financialreport.autoreport.FinancialReportImpl")) {
                System.out.print("Automatic Report ");
            } else if (list.equals("aisco.program.activity.ProgramImpl")) {
                System.out.print("Activity ");
            } else {
                System.out.println(featuresList);
            }
        }

    }

    public void productValidation(String product) throws Exception {
        //Write the features' constraints here
        if (featuresList.contains("aisco.financialreport.income.FinancialReportImpl") && featuresList.contains("aisco.program.activity.ProgramImpl")) {
            System.out.println("Product is valid.");
            System.out.println("Product " + product + " is succesfully created.");
        } else {
            throw new Exception("Mandatory features are not selected");
        }
    }

    public void addFeatures(Object object) {
        String features = object.getClass().getName();
        featuresList.add(features);
    }

}
