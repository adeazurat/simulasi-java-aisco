package aisco.program.internal;

import aisco.program.activity.ProgramDecorator;

public class ProgramImpl extends ProgramDecorator {

    public ProgramImpl()
    {
     System.out.println("Internal Program");
    }


    public ProgramImpl(Integer idProgram, String name, String description, String target)
    {
        super(idProgram, name, description, target);
    }


    public String toString(){
        return " Internal " + name + "";
    }
}
