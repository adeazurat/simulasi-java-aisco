package aisco.program.activity;

public class ProgramImpl extends ProgramComponent {
    //FeatureSelection featureSelection;

    public ProgramImpl()
    {
        System.out.println("Feature Regular Activity is selected");

    }

    public ProgramImpl(Integer idProgram, String name, String description, String target, String partner, String logoUrl)
    {
        super(idProgram, name, description, target, partner, logoUrl);
    }

    public String toString(){
        return " " + name + "";
    }
}

