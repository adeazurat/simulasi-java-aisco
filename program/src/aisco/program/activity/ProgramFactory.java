package aisco.program.activity;

import java.lang.reflect.Constructor;
import java.util.logging.Logger;

public class ProgramFactory {
    private static final Logger LOGGER = Logger.getLogger(ProgramFactory.class.getName());

    public ProgramFactory()
    {

    }

    public static Program createProgram(String fullyQualifiedName)
    {   Program record = null;
        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            Constructor<?> constructor = clz.getDeclaredConstructors()[0];
            //System.out.println(constructor);
            record = (Program) constructor.newInstance();
        } catch (Exception e)
        {
            System.out.println(e);
            System.out.println("Failed to create instance of Financial Report");
        }
        return record;
    }


    public static Program createProgram(String fullyQualifiedName, Integer idProgram, String name, String description, String target, String partner, String logoUrl) {
        Program record = null;
        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            //System.out.println(clz);
            Constructor<?> constructor1 = clz.getConstructor(Integer.class, String.class, String.class, String.class, String.class, String.class);
            record = (Program) constructor1.newInstance(idProgram, name, description, target, partner, logoUrl);
        }
        catch (NoSuchMethodException ex)
        {
            System.out.println("Method not found");
            System.exit(1);
        }
        catch (Exception ex) {
            //System.out.println("Hello Catch");
            System.out.println(ex);
            LOGGER.severe("Failed to create instance of Program. Returning base Program.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
        }

        return record;
    }

    public static Program createProgram(String fullyQualifiedName, Integer idProgram, String name, String description, String target) {
        Program record = null;
        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            //System.out.println(clz);
            Constructor<?> constructor2 = clz.getConstructor(Integer.class, String.class, String.class, String.class);
            record = (Program) constructor2.newInstance(idProgram, name, description, target);
        }
        catch (NoSuchMethodException ex)
        {
            System.out.println("Method not found");
            //throw new RuntimeException(ex);
            System.exit(1);
        }
        catch (Exception ex) {
            //System.out.println("Hello Catch");
            System.out.println(ex);
            LOGGER.severe("Failed to create instance of Program. Returning base Program.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
        }

        return record;
    }
}
