package aisco.financialreport.expense;

import aisco.program.activity.Program;
import aisco.financialreport.core.FinancialReportDecorator;

public class FinancialReportImpl extends FinancialReportDecorator {

    public FinancialReportImpl()
    {
        super();
    }

    public FinancialReportImpl(String id, String dateStamp, Integer amount, String description, Program idProgram, String idCoa)
    {
        super(id, dateStamp, amount, description, idProgram, idCoa);

    }

    @Override
    public void run()
    {
        System.out.println("Feature Expense Report is selected");
    }

 /*   public static void main(String[] args)
    {
        FinancialReport abc = FinancialReportFactory.createFinancialReport(FinancialReportImpl.class.getName());
        FinancialReport a = FinancialReportFactory.createFinancialReport(FinancialReportImpl.class.getName(), "10", "23-10-2019", 100000, "Beli Beras", "berbagi nasi", "41000");
        FinancialReport b = FinancialReportFactory.createFinancialReport(FinancialReportImpl.class.getName(), "20", "24-10-2019", 10000, "Kertas Nasi", "berbagi nasi", "410");
        ArrayList<FinancialReport> arrayexpense = new ArrayList<FinancialReport>();
        // FinancialReport a = new FinancialReportImpl("1", "23-10-2019", 100000, "Sumbangan bu Nana", "berbagi nasi", "11000");
//       FinancialReport b = new FinancialReportImpl("2", "24-10-2019", 10000, "Sumbangan Pak Joni", "berbagi nasi", "110");
        arrayexpense.add(a);
        arrayexpense.add(b);
        printListReport(arrayexpense);
    }*/


}



