package aisco.financialreport.income;

import aisco.program.activity.Program;
import aisco.financialreport.core.FinancialReportDecorator;

public class FinancialReportImpl extends FinancialReportDecorator {
    private String paymentMethod;
    private static int totalIncome;

    public FinancialReportImpl() {
        super();
    }

    public FinancialReportImpl(String id, String dateStamp, Integer amount, String description, Program idProgram, String idCoa, String paymentMethod) {
        super(id, dateStamp, amount, description, idProgram, idCoa);
        this.paymentMethod = paymentMethod;
        addAmount(amount);
    }

    @Override
    public void run() {
        System.out.println("Feature Income Report is selected");
    }

    public static void addAmount(int amount) {

        totalIncome+=amount;

    }

    public static void printTotalIncome()
    {
        System.out.println("Total income adalah: "+totalIncome);
    }

    public String toString() {
        return "- " + description + ": " + amount + " untuk Program" + idProgram + ". Metode Pembayaran: " + paymentMethod + "\n";
    }

/*
    public static void main(String[] args) {
        FinancialReport abc = FinancialReportFactory.createFinancialReport(FinancialReportImpl.class.getName());
        FinancialReport a = FinancialReportFactory.createFinancialReport(FinancialReportImpl.class.getName(), "1", "23-10-2019", 100000, "Sumbangan bu Nana", "berbagi nasi", "11000");
        FinancialReport b = FinancialReportFactory.createFinancialReport(FinancialReportImpl.class.getName(), "2", "24-10-2019", 10000, "Sumbangan Pak Joni", "berbagi nasi", "110");
        ArrayList<FinancialReport> arrayreport = new ArrayList<FinancialReport>();
        arrayreport.add(a);
        arrayreport.add(b);
        printListReport(arrayreport);

    }
*/


}


