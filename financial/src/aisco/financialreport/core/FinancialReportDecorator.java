package aisco.financialreport.core;

import aisco.program.activity.Program;

public abstract class FinancialReportDecorator extends FinancialReportComponent {

    public FinancialReportDecorator() {
        super();
    }

    public FinancialReportDecorator(String id, String dateStamp, Integer amount, String description, Program idProgram, String idCoa) {
        super(id, dateStamp, amount, description, idProgram, idCoa);

    }


    @Override
    public void run()
    {
        System.out.println("Hai");
    }

}
