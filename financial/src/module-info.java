module aisco.financialreport {
    exports aisco.financialreport.income;
    exports aisco.financialreport.core;
    requires java.logging;
    requires aisco.program;
}